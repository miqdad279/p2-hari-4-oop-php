<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    
    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name."<br>";
    echo "legs : ".$sheep->legs."<br>"; 
    echo "Cold blooded : ".$sheep->cold_blooded."<br><br>"; 

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name."<br>";
    echo "legs : ".$sheep->legs."<br>";
    echo "Cold blooded : ".$sheep->cold_blooded."<br>";
    echo $kodok->jump("Hop Hop")."<br><br>"; 

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name."<br>";
    echo "legs : ".$sungokong->legs."<br>";
    echo "Cold blooded : ".$sheep->cold_blooded."<br>";
    echo $sungokong->yell("Auooo");

?>